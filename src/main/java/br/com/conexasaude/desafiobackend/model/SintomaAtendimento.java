package br.com.conexasaude.desafiobackend.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Entity
@Data @EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class SintomaAtendimento {

	@Id @GeneratedValue(strategy = GenerationType.IDENTITY)
	@EqualsAndHashCode.Include
	private Long id;
	
	@ManyToOne
	private Atendimento atendimento;
	
	private String descricao;
	private String detalhes;
	
	public SintomaAtendimento() {		
	}

	public SintomaAtendimento(Atendimento atendimento, String descricao, String detalhes) {
		super();
		this.atendimento = atendimento;
		this.descricao = descricao;
		this.detalhes = detalhes;
	}	
}
