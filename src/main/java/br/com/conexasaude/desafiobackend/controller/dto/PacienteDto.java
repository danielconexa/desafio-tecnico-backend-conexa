package br.com.conexasaude.desafiobackend.controller.dto;

import org.springframework.beans.BeanUtils;

import br.com.conexasaude.desafiobackend.model.Paciente;
import lombok.Data;

@Data
public class PacienteDto {

	private Long id;
	private String nome;
	private String cpf;
	private Integer idade;
	private String email;
	private String telefone;
	
	public PacienteDto() {
	}
	
	public PacienteDto(Paciente paciente) {
		BeanUtils.copyProperties(paciente, this);
	}
}
