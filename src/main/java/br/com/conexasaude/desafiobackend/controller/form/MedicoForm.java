package br.com.conexasaude.desafiobackend.controller.form;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

import br.com.conexasaude.desafiobackend.model.Medico;
import lombok.Data;

@Data
public class MedicoForm {
	
	@NotEmpty @Email
	private String email;
	@NotEmpty
	private String senha;
	@NotEmpty
	private String confirmacaoSenha;
	private String specialidade;
	@NotEmpty
	private String cpf;
	@Positive
	private String idade;
	private String telefone;
	
	public Medico converter() {
        Medico medico = new Medico();
        medico.setEmail(email);
        medico.setSenha(senha);
        medico.setSpecialidade(specialidade);
        medico.setCpf(cpf);
        medico.setIdade(Integer.parseInt(idade));
        medico.setTelefone(telefone);
        return medico;
    }
}
