package br.com.conexasaude.desafiobackend.controller.form;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import br.com.conexasaude.desafiobackend.model.Paciente;
import lombok.Data;

@Data
public class PacienteForm {

	@NotEmpty
	private String nome;
	@NotNull
	private String cpf;
	@Positive
	private String idade;
	@Email
	private String email;
	private String telefone;
	
	public Paciente converter() {
        Paciente paciente = new Paciente();
        paciente.setNome(nome);
        paciente.setCpf(cpf);
        paciente.setIdade(Integer.parseInt(idade));
        paciente.setEmail(email);
        paciente.setTelefone(telefone);
        return paciente;
    }
}
