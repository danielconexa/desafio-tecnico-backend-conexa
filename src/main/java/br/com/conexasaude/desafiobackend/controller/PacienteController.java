package br.com.conexasaude.desafiobackend.controller;

import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.conexasaude.desafiobackend.config.exceptionhandling.RetornoJsonDto;
import br.com.conexasaude.desafiobackend.controller.dto.PacienteDto;
import br.com.conexasaude.desafiobackend.controller.form.PacienteForm;
import br.com.conexasaude.desafiobackend.model.Paciente;
import br.com.conexasaude.desafiobackend.service.PacienteService;
import br.com.conexasaude.desafiobackend.util.CpfCnpjUtils;

@RestController
@RequestMapping("/api/v1/patients")
public class PacienteController {

	@Autowired
	private PacienteService pacienteService;
	
	@PostMapping
	public ResponseEntity<?> incluir(@RequestBody @Valid PacienteForm form, UriComponentsBuilder uriBuilder) {
		if (!CpfCnpjUtils.isCpfValido(form.getCpf())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new RetornoJsonDto("CPF inválido!"));
		}
		Paciente paciente = pacienteService.incluir(form.converter());

		URI uri = uriBuilder.path("/pacientes/{id}").buildAndExpand(paciente.getId()).toUri();
		return ResponseEntity.created(uri).body(new PacienteDto(paciente));
	}

	@PutMapping("/{id}")
	public ResponseEntity<?> alterar(@PathVariable Long id, @RequestBody @Valid PacienteForm form) {
		if (!CpfCnpjUtils.isCpfValido(form.getCpf())) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new RetornoJsonDto("CPF inválido!"));
		}
		Paciente paciente = pacienteService.alterar(id, form.converter());
		return ResponseEntity.ok(new PacienteDto(paciente));
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<PacienteDto> excluir(@PathVariable Long id) {
		pacienteService.excluir(id);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<PacienteDto> detalhar(@PathVariable Long id) {
		Optional<Paciente> optional = pacienteService.obterAtivo(id);
		if (optional.isEmpty()) {
			return ResponseEntity.notFound().build();
		}
		return ResponseEntity.ok(new PacienteDto(optional.get()));
	}
	
	@GetMapping
	public List<?> listar() {
		List<Paciente> pacientes = pacienteService.listar();
		return pacientes.stream().map(PacienteDto::new).collect(Collectors.toList());
	}
}
