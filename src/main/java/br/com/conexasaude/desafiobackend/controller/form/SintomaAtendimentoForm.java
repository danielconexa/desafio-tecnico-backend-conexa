package br.com.conexasaude.desafiobackend.controller.form;

import javax.validation.constraints.NotEmpty;

import lombok.Data;

@Data
public class SintomaAtendimentoForm {

	@NotEmpty
	private String descricao;
	private String detalhes;
}
