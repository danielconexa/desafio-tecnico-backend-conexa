package br.com.conexasaude.desafiobackend.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.conexasaude.desafiobackend.config.exceptionhandling.RetornoJsonDto;
import br.com.conexasaude.desafiobackend.controller.dto.TokenDto;
import br.com.conexasaude.desafiobackend.controller.form.LoginForm;
import br.com.conexasaude.desafiobackend.service.TokenService;

@RestController
@RequestMapping("/api/v1")
public class AutenticacaoController {
	
	@Autowired
	private AuthenticationManager authManager;
	
	@Autowired
	private TokenService tokenService;

	@PostMapping
	@RequestMapping("/login")
	public ResponseEntity<TokenDto> login(@RequestBody @Valid LoginForm form) {
		UsernamePasswordAuthenticationToken dadosLogin = form.converter();
		
		try {
			Authentication authentication = authManager.authenticate(dadosLogin);
			String token = tokenService.gerarToken(authentication);
			return ResponseEntity.ok(new TokenDto(token, "Bearer"));
		} catch (AuthenticationException e) {
			return ResponseEntity.badRequest().build();
		}
	}

	@PostMapping
	@RequestMapping("/logoff")
	public ResponseEntity<RetornoJsonDto> logoff(HttpServletRequest request) {
		String token = request.getHeader("Authorization").replace("Bearer ", ""); 
		tokenService.logoff(token);
		return ResponseEntity.ok(new RetornoJsonDto("Token invalidado com sucesso!"));
	}	
}
