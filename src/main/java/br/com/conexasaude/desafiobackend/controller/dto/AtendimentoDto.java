package br.com.conexasaude.desafiobackend.controller.dto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import br.com.conexasaude.desafiobackend.model.Atendimento;
import lombok.Data;

@Data
public class AtendimentoDto {

	private Long id;
	private LocalDateTime dataHora;
	private Long idPaciente;
	private List<SintomaAtendimentoDto> sintomas = new ArrayList<SintomaAtendimentoDto>();
	
	public AtendimentoDto() {
	}
	
	public AtendimentoDto(Atendimento atendimento) {
		this.id = atendimento.getId();
		this.idPaciente = atendimento.getPaciente().getId();
		this.dataHora = atendimento.getDataHoraAtendimento();
		this.sintomas = atendimento.getSintomas()
				.stream()
				.map(s -> new SintomaAtendimentoDto(s.getDescricao(), s.getDetalhes()))
				.collect(Collectors.toList());		
	}
}
