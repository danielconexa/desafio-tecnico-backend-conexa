package br.com.conexasaude.desafiobackend.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.conexasaude.desafiobackend.model.TokenInvalidado;

public interface TokenInvalidadoRepository extends JpaRepository<TokenInvalidado, Long> {

	Optional<String> findByToken(String token);
}
