package br.com.conexasaude.desafiobackend.service;

import java.util.Date;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import br.com.conexasaude.desafiobackend.model.Medico;
import br.com.conexasaude.desafiobackend.model.TokenInvalidado;
import br.com.conexasaude.desafiobackend.repository.TokenInvalidadoRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class TokenService {
	
	@Value("${conexa.jwt.expiration}")
	private String expiration;
	
	@Value("${conexa.jwt.secret}")
	private String secret;
	
	@Autowired
	private TokenInvalidadoRepository tokenInvalidadoRepository;

	public String gerarToken(Authentication authentication) {
		Medico logado = (Medico) authentication.getPrincipal();
		Date hoje = new Date();
		Date dataExpiracao = new Date(hoje.getTime() + Long.parseLong(expiration));
		
		return Jwts.builder()
				.setIssuer("Conexa")
				.setSubject(logado.getId().toString())
				.setIssuedAt(hoje)
				.setExpiration(dataExpiracao)
				.signWith(SignatureAlgorithm.HS256, secret)
				.compact();
	}

	public boolean isTokenValido(String token) {
		try {
			// Checa validade das claims do token
			Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token);
			// Checa se o token foi invalidado através de logoff
			return !isTokenInvalidado(token);
		} catch (Exception e) {
			return false;
		}
	}

	public Long getIdUsuario(String token) {
		Claims claims = Jwts.parser().setSigningKey(this.secret).parseClaimsJws(token).getBody();
		return Long.parseLong(claims.getSubject());
	}
	
	@Transactional
	public void logoff(String token) {
		TokenInvalidado tokenInvalidado = new TokenInvalidado(token);
		tokenInvalidadoRepository.save(tokenInvalidado);
	}
	
	private boolean isTokenInvalidado(String token) {
		return tokenInvalidadoRepository.findByToken(token).isPresent();
	}
}
