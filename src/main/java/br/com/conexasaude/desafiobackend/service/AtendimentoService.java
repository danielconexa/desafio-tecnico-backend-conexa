package br.com.conexasaude.desafiobackend.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conexasaude.desafiobackend.config.exceptionhandling.exceptions.ErroNegocioException;
import br.com.conexasaude.desafiobackend.config.exceptionhandling.exceptions.NaoEncontradoException;
import br.com.conexasaude.desafiobackend.model.Atendimento;
import br.com.conexasaude.desafiobackend.model.Medico;
import br.com.conexasaude.desafiobackend.model.Paciente;
import br.com.conexasaude.desafiobackend.repository.AtendimentoRepository;

@Service
public class AtendimentoService {

	@Autowired
	private AtendimentoRepository atendimentoRepository;

	@Autowired
	private AutenticacaoService autenticacaoService;
	
	@Autowired
	private PacienteService pacienteService;

	@Transactional
	public Atendimento incluir(Atendimento atendimento) {
		Optional<Paciente> paciente = pacienteService.obterAtivo(atendimento.getPaciente().getId());
		if (paciente.isEmpty()) {
			throw new ErroNegocioException("Não existe um paciente ativo com o ID informado!");
		}
		atendimento.setMedico(autenticacaoService.obterMedicoLogado());
		return atendimentoRepository.save(atendimento);
	}

	@Transactional
	public void excluir(Long id) {
		Optional<Atendimento> optional = atendimentoRepository.findById(id);
		
		if (optional.isEmpty()) {
			throw new NaoEncontradoException();
		}
		Medico medicoLogado = autenticacaoService.obterMedicoLogado();
		
		if (!medicoLogado.getId().equals(optional.get().getMedico().getId())) {
			throw new ErroNegocioException("Não é possível cancelar pois este atendimento foi criado por outro usuário!");
		}
		atendimentoRepository.deleteById(id);
	}
	
	public List<Atendimento> listarFuturos() {
		Medico medicoLogado = autenticacaoService.obterMedicoLogado();
		return atendimentoRepository.listarAtendimentosAposData(medicoLogado.getId(), LocalDateTime.now());
	}
}
