package br.com.conexasaude.desafiobackend.service;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.conexasaude.desafiobackend.config.exceptionhandling.exceptions.ErroNegocioException;
import br.com.conexasaude.desafiobackend.config.exceptionhandling.exceptions.NaoEncontradoException;
import br.com.conexasaude.desafiobackend.model.Paciente;
import br.com.conexasaude.desafiobackend.repository.PacienteRepository;

@Service
public class PacienteService {

	@Autowired
	private PacienteRepository pacienteRepository;
	
	@Transactional
	public Paciente incluir(Paciente paciente) {
		Optional<Paciente> pacienteMesmoCpf = pacienteRepository.findByCpf(paciente.getCpf());
		
		if (pacienteMesmoCpf.isPresent()) {
			// Reativação (existia no banco com status inativo)
			if (!pacienteMesmoCpf.get().getAtivo()) {
				alterarCampos(paciente, pacienteMesmoCpf.get());
				pacienteMesmoCpf.get().setAtivo(Boolean.TRUE);
			} else {
				throw new ErroNegocioException("Já existe um paciente cadastrado com este mesmo CPF!");
			}
		}
		return pacienteRepository.save(paciente);
	}
	
	@Transactional
	public Paciente alterar(Long id, Paciente pacienteInformado) {
		Optional<Paciente> pacienteBanco = obterAtivo(id);

		if (pacienteBanco.isEmpty()) {
			throw new NaoEncontradoException();
		}

		boolean alterouCpf = !pacienteInformado.getCpf().equals(pacienteBanco.get().getCpf());

		// Se alterou o CPF, não pode ficar igual ao de outro paciente
		if (alterouCpf && pacienteRepository.findByCpf(pacienteInformado.getCpf()).isPresent()) {
			throw new ErroNegocioException("Já existe outro paciente com este mesmo CPF!");
		}
		alterarCampos(pacienteInformado, pacienteBanco.get());
		return pacienteBanco.get();
	}

	@Transactional
	public void excluir(Long id) {
		if (obterAtivo(id).isEmpty()) {
			throw new NaoEncontradoException();
		}
		pacienteRepository.deleteById(id);
	}

	public List<Paciente> listar() {
		return pacienteRepository.findAllByAtivo(Boolean.TRUE);
	}

	public Optional<Paciente> obterAtivo(Long id) {
		return pacienteRepository.findById(id)
				.filter(p -> p.getAtivo());
	}
	
	private void alterarCampos(Paciente pacienteInformado, Paciente pacienteBanco) {
		pacienteBanco.setNome(pacienteInformado.getNome());
		pacienteBanco.setCpf(pacienteInformado.getCpf());
		pacienteBanco.setIdade(pacienteInformado.getIdade());
		pacienteBanco.setEmail(pacienteInformado.getEmail());
		pacienteBanco.setTelefone(pacienteInformado.getTelefone());
	}
}
