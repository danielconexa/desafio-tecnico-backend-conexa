package br.com.conexasaude.desafiobackend.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import br.com.conexasaude.desafiobackend.model.Medico;
import br.com.conexasaude.desafiobackend.repository.MedicoRepository;

@Service
public class AutenticacaoService implements UserDetailsService {
	
	@Autowired
	private MedicoRepository repository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Optional<Medico> usuario = repository.findByEmail(username);
		if (usuario.isPresent()) {
			return usuario.get();
		}
		
		throw new UsernameNotFoundException("Dados inválidos!");
	}
	
	public Medico obterMedicoLogado() {
		return (Medico) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

}
