package br.com.conexasaude.desafiobackend.controller;

import java.net.URI;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class PacienteControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void deveriaDevolver201AoIncluirPacienteComDadosCorretos() throws Exception {
		signup("medicoincluirpacientedadoscorretos@gmail.com", "1234");
		String token = login("medicoincluirpacientedadoscorretos@gmail.com", "1234");
		
		URI uri = new URI("/api/v1/patients");
		
		JSONObject json = new JSONObject();
		json.put("nome", "Paciente Teste");
		json.put("cpf", "682.309.550-42");
		json.put("idade", "22");
		json.put("email", "pacienteatendimento@gmail.com");
		json.put("telefone", "(21) 4421-6267");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());
	}

	@Test
	public void deveriaDevolver400AoIncluirPacienteComCpfInvalido() throws Exception {
		signup("medicoincluirpacientecpfinvalido@gmail.com", "1234");
		String token = login("medicoincluirpacientecpfinvalido@gmail.com", "1234");
		
		URI uri = new URI("/api/v1/patients");
		
		JSONObject json = new JSONObject();
		json.put("nome", "Paciente Teste");
		json.put("cpf", "111.222.333-44");
		json.put("idade", "22");
		json.put("email", "pacienteatendimento@gmail.com");
		json.put("telefone", "(21) 4421-6267");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	public void deveriaDevolver400AoIncluirPacienteComCpfJaExistente() throws Exception {
		signup("medicoincluirpacientecpfjaexistente@gmail.com", "1234");
		String token = login("medicoincluirpacientecpfjaexistente@gmail.com", "1234");
		
		URI uri = new URI("/api/v1/patients");
		
		JSONObject json = new JSONObject();
		json.put("nome", "Paciente Teste");
		json.put("cpf", "414.345.310-07");
		json.put("idade", "22");
		json.put("email", "pacienteatendimento@gmail.com");
		json.put("telefone", "(21) 4421-6267");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	public void deveriaDevolver200AoAlterarPacienteCorretamente() throws Exception {
		signup("medicoalterarpacientedadoscorretos@gmail.com", "1234");
		String token = login("medicoalterarpacientedadoscorretos@gmail.com", "1234");
		
		URI uri = new URI("/api/v1/patients");
		
		JSONObject json = new JSONObject();
		json.put("nome", "Robreto Catsro");
		json.put("cpf", "740.265.650-09");
		json.put("idade", "22");
		json.put("email", "pacienteatendimento@gmail.com");
		json.put("telefone", "(21) 4421-6267");
		
		String bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString();
		
		String idPaciente = new JSONObject(bodyResposta).get("id").toString();
		
		uri = new URI("/api/v1/patients/" + idPaciente);
		
		json.put("nome", "Roberto Castro");
		
		bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.put(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		
		String nome = new JSONObject(bodyResposta).get("nome").toString();
		Assert.assertEquals(nome, "Roberto Castro");
	}

	@Test
	public void deveriaDevolver400AoAlterarCpfPacienteParaMesmoOutroPaciente() throws Exception {
		signup("medicoalterarcpfpacientemesmooutro@gmail.com", "1234");
		String token = login("medicoalterarcpfpacientemesmooutro@gmail.com", "1234");
		String cpfPaciente1 = "950.378.940-00";
		
		URI uri = new URI("/api/v1/patients");
		
		// Paciente 1
		JSONObject json = new JSONObject();
		json.put("nome", "Paciente Teste");
		json.put("cpf", cpfPaciente1);
		json.put("idade", "22");
		json.put("email", "pacienteatendimento@gmail.com");
		json.put("telefone", "(21) 4421-6267");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());

		// Paciente 2
		json = new JSONObject();
		json.put("nome", "Paciente Teste");
		json.put("cpf", "572.557.260-97");
		json.put("idade", "22");
		json.put("email", "pacienteatendimento@gmail.com");
		json.put("telefone", "(21) 4421-6267");
		
		String bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString();
		
		json.put("cpf", cpfPaciente1);
		
		String idPaciente2 = new JSONObject(bodyResposta).get("id").toString();
		
		uri = new URI("/api/v1/patients/" + idPaciente2);
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.put(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void deveriaDevolver200AoExcluirPacienteCorretamente() throws Exception {
		signup("medicoexcluirpacientecorretamente@gmail.com", "1234");
		String token = login("medicoexcluirpacientecorretamente@gmail.com", "1234");
		
		URI uri = new URI("/api/v1/patients");
		
		JSONObject json = new JSONObject();
		json.put("nome", "Paciente Teste");
		json.put("cpf", "278.016.740-81");
		json.put("idade", "22");
		json.put("email", "pacienteatendimento@gmail.com");
		json.put("telefone", "(21) 4421-6267");
		
		String bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString();

		String idPaciente = new JSONObject(bodyResposta).get("id").toString();
		
		uri = new URI("/api/v1/patients/" + idPaciente);
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.delete(uri)
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk());
	}
	
	@Test
	public void deveriaDevolver404AoExcluirPacienteNaoExistente() throws Exception {
		signup("medicoexcluirpacientenaoexistente@gmail.com", "1234");
		String token = login("medicoexcluirpacientenaoexistente@gmail.com", "1234");
		
		URI uri = new URI("/api/v1/patients/" + "951984");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.delete(uri)
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isNotFound());
	}
	
	@Test
	public void deveriaDevolverDadosCorretosAoDetalharPacienteCadastrado() throws Exception {
		signup("medicodetalharpacientecadastrado@gmail.com", "1234");
		String token = login("medicodetalharpacientecadastrado@gmail.com", "1234");
		
		URI uri = new URI("/api/v1/patients");
		
		JSONObject jsonPacienteCadastrado = new JSONObject();
		jsonPacienteCadastrado.put("nome", "Paciente Teste");
		jsonPacienteCadastrado.put("cpf", "005.762.170-50");
		jsonPacienteCadastrado.put("idade", 22);
		jsonPacienteCadastrado.put("email", "pacienteatendimento@gmail.com");
		jsonPacienteCadastrado.put("telefone", "(21) 4421-6267");
		
		String bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(jsonPacienteCadastrado.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString();
		
		String idPaciente = new JSONObject(bodyResposta).get("id").toString();

		uri = new URI("/api/v1/patients/" + idPaciente);
		
		bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.get(uri)
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		
		JSONObject jsonResposta = new JSONObject(bodyResposta);
		Assert.assertEquals(jsonResposta.get("nome"), jsonPacienteCadastrado.get("nome"));
		Assert.assertEquals(jsonResposta.get("cpf"), jsonPacienteCadastrado.get("cpf"));
		Assert.assertEquals(jsonResposta.get("idade"), jsonPacienteCadastrado.get("idade"));
		Assert.assertEquals(jsonResposta.get("email"), jsonPacienteCadastrado.get("email"));
		Assert.assertEquals(jsonResposta.get("telefone"), jsonPacienteCadastrado.get("telefone"));
	}
	
	@Test
	public void deveriaDevolverDadosCorretosPacienteAoListarTodosPacientes() throws Exception {
		signup("medicolistartodospacientes@gmail.com", "1234");
		String token = login("medicolistartodospacientes@gmail.com", "1234");
		
		URI uri = new URI("/api/v1/patients");
		
		JSONObject jsonPacienteCadastrado = new JSONObject();
		jsonPacienteCadastrado.put("nome", "Paciente Teste");
		jsonPacienteCadastrado.put("cpf", "848.506.480-10");
		jsonPacienteCadastrado.put("idade", 22);
		jsonPacienteCadastrado.put("email", "pacienteatendimento@gmail.com");
		jsonPacienteCadastrado.put("telefone", "(21) 4421-6267");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(jsonPacienteCadastrado.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());
		
		String bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.get(uri)
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		
		JSONArray arrayResposta = new JSONArray(bodyResposta);
		
		boolean encontrou = false;
		for (int i = 0; i < arrayResposta.length(); i++) {
			JSONObject jsonPacienteResposta = arrayResposta.getJSONObject(i);
			String cpfPacienteResposta = jsonPacienteResposta.get("cpf").toString();
			if (cpfPacienteResposta.equals(jsonPacienteCadastrado.get("cpf"))) {
				Assert.assertEquals(jsonPacienteResposta.get("nome"), jsonPacienteCadastrado.get("nome"));
				Assert.assertEquals(jsonPacienteResposta.get("cpf"), jsonPacienteCadastrado.get("cpf"));
				Assert.assertEquals(jsonPacienteResposta.get("idade"), jsonPacienteCadastrado.get("idade"));
				Assert.assertEquals(jsonPacienteResposta.get("email"), jsonPacienteCadastrado.get("email"));
				Assert.assertEquals(jsonPacienteResposta.get("telefone"), jsonPacienteCadastrado.get("telefone"));
				encontrou = true;
				break;
			}
		}
		Assert.assertTrue(encontrou);
	}
	
	private void signup(String email, String senha) throws Exception {
		URI uri = new URI("/api/v1/signup");
		
		JSONObject jsonSignup = new JSONObject();
		jsonSignup.put("email", email);
		jsonSignup.put("senha", senha);
		jsonSignup.put("confirmacaoSenha", senha);
		jsonSignup.put("specialidade", "Cardiologista");
		jsonSignup.put("cpf", "993.077.970-13");
		jsonSignup.put("idade", "33");
		jsonSignup.put("telefone", "(21) 3232-6565");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(jsonSignup.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());
	}
	
	private String login(String email, String senha) throws Exception {
		URI uri = new URI("/api/v1/login");
		
		JSONObject json = new JSONObject();
		json.put("email", email);
		json.put("senha", senha);
		
		String bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		
		return new JSONObject(bodyResposta).get("token").toString();
	}
}
