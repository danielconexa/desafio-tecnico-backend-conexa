package br.com.conexasaude.desafiobackend.controller;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class AtendimentoControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@Test
	public void deveriaDevolver201AoCadastrarAtendimentoComDadosCorretos() throws Exception {
		signup("medicoatendimentocorreto@gmail.com", "1234");
		String token = login("medicoatendimentocorreto@gmail.com", "1234");
		String idPaciente = incluirPaciente(token, "476.810.680-31");
		
		URI uri = new URI("/api/v1/attendance");
		
		JSONObject json = new JSONObject();
		json.put("dataHora", LocalDateTime.now().plus(1, ChronoUnit.HOURS).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		json.put("idPaciente", idPaciente);
		json.accumulate("sintomas", jsonSintoma("Dor de cabeça", "Dor na parte de trás da cabeça"));
		json.accumulate("sintomas", jsonSintoma("Febre", "40 graus de febre"));
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());
	}
	
	@Test
	public void deveriaDevolver400AoCancelarAtendimentoCriadoPorOutroMedico() throws Exception {
		signup("testecancelamentomedico1@gmail.com", "1234");
		String token = login("testecancelamentomedico1@gmail.com", "1234");
		String idPaciente = incluirPaciente(token, "884.133.370-70");
		
		URI uri = new URI("/api/v1/attendance");
		
		JSONObject json = new JSONObject();
		json.put("dataHora", LocalDateTime.now().plus(1, ChronoUnit.HOURS).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		json.put("idPaciente", idPaciente);
		json.accumulate("sintomas", jsonSintoma("Dor de cabeça", "Dor na parte de trás da cabeça"));
		json.accumulate("sintomas", jsonSintoma("Febre", "40 graus de febre"));
		
		String bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString();
		
		String idAtendimento = new JSONObject(bodyResposta).get("id").toString();
		signup("testecancelamentomedico2@gmail.com", "1234");
		token = login("testecancelamentomedico2@gmail.com", "1234");

		uri = new URI("/api/v1/attendance/" + idAtendimento);
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.delete(uri)
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isBadRequest());
	}
	
	@Test
	public void naoDeveriaEncontrarAtendimentoCriadoPorOutroMedico() throws Exception {
		signup("testelistarmedico1@gmail.com", "1234");
		String token = login("testelistarmedico1@gmail.com", "1234");
		String idPaciente = incluirPaciente(token, "138.077.790-94");
		
		URI uri = new URI("/api/v1/attendance");
		
		JSONObject json = new JSONObject();
		json.put("dataHora", LocalDateTime.now().plus(1, ChronoUnit.HOURS).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		json.put("idPaciente", idPaciente);
		json.accumulate("sintomas", jsonSintoma("Dor de cabeça", "Dor na parte de trás da cabeça"));
		json.accumulate("sintomas", jsonSintoma("Febre", "40 graus de febre"));
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());
		
		signup("testelistarmedico2@gmail.com", "1234");
		token = login("testelistarmedico2@gmail.com", "1234");

		uri = new URI("/api/v1/attendance/");
		
		String bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.get(uri)
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		
		JSONArray arrayResposta = new JSONArray(bodyResposta);
		Assert.assertTrue(arrayResposta.length() == 0);
	}
	
	private void signup(String email, String senha) throws Exception {
		URI uri = new URI("/api/v1/signup");
		
		JSONObject jsonSignup = new JSONObject();
		jsonSignup.put("email", email);
		jsonSignup.put("senha", senha);
		jsonSignup.put("confirmacaoSenha", senha);
		jsonSignup.put("specialidade", "Cardiologista");
		jsonSignup.put("cpf", "993.077.970-13");
		jsonSignup.put("idade", "33");
		jsonSignup.put("telefone", "(21) 3232-6565");
		
		mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(jsonSignup.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated());
	}
	
	private String login(String email, String senha) throws Exception {
		URI uri = new URI("/api/v1/login");
		
		JSONObject json = new JSONObject();
		json.put("email", email);
		json.put("senha", senha);
		
		String bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isOk()).andReturn().getResponse().getContentAsString();
		
		return new JSONObject(bodyResposta).get("token").toString();
	}
	
	private String incluirPaciente(String token, String cpf) throws Exception {
		URI uri = new URI("/api/v1/patients");
		
		JSONObject json = new JSONObject();
		json.put("nome", "Paciente Teste Atendimento");
		json.put("cpf", cpf);
		json.put("idade", "22");
		json.put("email", "pacienteatendimento@gmail.com");
		json.put("telefone", "(21) 4421-6267");
		     
		String bodyResposta = mockMvc
		.perform(MockMvcRequestBuilders
				.post(uri)
				.content(json.toString())
				.header("Authorization", "Bearer " + token)
				.contentType(MediaType.APPLICATION_JSON))
		.andExpect(MockMvcResultMatchers.status().isCreated()).andReturn().getResponse().getContentAsString();
		
		return new JSONObject(bodyResposta).get("id").toString();
	}

	private JSONObject jsonSintoma(String descricao, String detalhes) throws JSONException {
		return new JSONObject()
				.put("descricao", descricao)
				.put("detalhes", detalhes);
	}
}
